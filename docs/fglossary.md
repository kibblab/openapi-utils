# Field Glossary

This script gathers every property name used in JSON schemas. The primary purpose is to establish API Naming Conventions, and see how people name their existing fields.

The script also gets the name of parameters[] from OpenAPI schema.

In PowerShell:

```powershell
jq -rf glossary.jq --slurp (Get-ChildItem schemas\*.json).FullName
```

In bash: TODO

Outputs a tsv sorted by frequency of field names

```
id      12
name    9
status  6
type    4
message 4
username        3
userStatus      2
tags    2
source  2
shipDate        2
quantity        2
photoUrls       2
...
```

## Extended

Use `--arg x` and get the details like type and descriptions.

```
jq -rf glossary.jq --arg x --slurp schemas\*.json
```

Output is a YAML file in alphetical order:

```
photoUrls:
  - description: The list of URL to a cute photos featuring pet
    type: array
    maxItems: 20
    xml: {"name":"photoUrl","wrapped":true}
    items: {"type":"string","format":"url"}
prop1:
  - type: string
    description: Dumb Property
quantity:
  - type: integer
    format: int32
    minimum: 1
    default: 1
  - type: integer
    format: int32
    example: 7
rqeuestId:
  - description: Unique Request Id
    type: string
    writeOnly: true
shipDate:
  - description: Estimated ship date
    type: string
    format: date-time
state:
  - type: string
    example: CA
status:
  - type: string
    description: Order Status
    enum: ["placed","approved","delivered"]
    example: approved
  - type: string
    description: Order Status
    enum: ["placed","approved","delivered"]
street:
  - type: string
    example: 437 Lytton
sub:
  - description: Test Sub Category
    type: object
    properties: {"prop1":{"type":"string","description":"Dumb Property"}}
tags:
  - description: Tags attached to the pet
    type: array
    minItems: 1
    xml: {"name":"tag","wrapped":true}
    items: {"$ref":"#/components/schemas/Tag"}
type:
  - type: string
  - type: string
userStatus:
  - type: integer
    format: int32
    example: 1
    description: User Status
  - description: User status
    type: integer
    format: int32
username:
  - description: User supplied username
    type: string
    minLength: 4
    example: John78
zip:
  - type: string
    example: 94301
```
