# Miscellaneous

> If using Powershell and writing one-liner scripts, you must escape `"` with triple quotes:
> 
> ```ps
> jq 'include """util"""; flatten_dot' myfile.json
> ```

Unix shells use standard quoting. The rest of this page follows this.

```sh
jq 'include "util"; flatten_dot' myfile.json
```

### Merge JSON

Simple trick using `--slurp` (`-s`) to input multiple files.

```sh
jq -s '.[0] * .[1]' <file1> <file2>
```

### List all operations

Generate a list of operations. Set `--raw-output` (`-r`) because we don't want quotes displayed.

```sh
jq -r -f list-operations.jq schemas/petstore.json
```
Result:

```text
post /pet
put /pet
get /pet/findByStatus
get /pet/findByTags
  ...
```

## util.jq

### Mark all endpoints as `x-internal`

```sh
jq 'include "util"; mark_all_internal' schemas/petstore.json
```

### Get the object of $ref

Local refs only. Set an `--arg` called `ref` to `#/components/schemas/Pet`:

```sh
jq --arg ref "#/components/schemas/Pet" 'include "util"; get_ref($ref)' schemas/petstore.json
```

Directly input the string into `get_ref()`:

```sh
jq 'include "util"; get_ref("#/components/schemas/Pet")' schemas/petstore.json
```

Result:

```text
{
  "type": "object",
  "required": [
    "name"
  ],
  "properties": {
    "name": {
      "type": "string"
    },
    "address": {
      "$ref": "#/components/schemas/Address"
    },
    "age": {
      "type": "integer",
      "format": "int32",
      "minimum": 0
    }
  }
}
```

### Flatten schema

Flatten all keys to dot notation. Outputs a single JSON object.

```sh
jq 'include "util"; flatten_dot' schemas/petstore.json > output.json
```
