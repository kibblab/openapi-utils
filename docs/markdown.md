# Output Markdown

Creates a Markdown page for a schema. Produces [PyMdown Extended](https://facelessuser.github.io/pymdown-extensions/)-flavored Markdown (used in MkDocs).

```sh
jq -L "print" -rf markdown.jq "schemas/petstore.json" > "output.md"
```

## Example

Go to [Petstore Example](./markdown_example.md)

## Limitations

- Cannot handle external $refs