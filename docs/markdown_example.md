# Swagger Petstore - OpenAPI 3.0


## Add a new pet to the store

Add a new pet to the store

```
POST /pet
```

#### Body

Create a new pet in the store

=== "json"

    <a href="#pet">Pet</a>

    ```json
    {
      "value": {
        "id": 10,
        "name": "doggie-request",
        "category": {
          "id": 1,
          "name": "Dogs"
        },
        "photoUrls": [
          "string"
        ],
        "tags": [
          {
            "id": 0,
            "name": "string"
          }
        ],
        "status": "available"
      }
    }
    ```



=== "xml"

    <a href="#pet">Pet</a>


=== "x-www-form-urlencoded"

    <a href="#pet">Pet</a>


#### Response


=== "200"

    Successful operation

    Returns <a href="#pet">Pet</a>

    **example-1**

    ```json
    {
      "value": {
        "id": 10,
        "name": "doggie",
        "category": {
          "id": 1,
          "name": "Dogs"
        },
        "photoUrls": [
          "string"
        ],
        "tags": [
          {
            "id": 0,
            "name": "string"
          }
        ],
        "status": "available"
      }
    }
    ```

    **example-2**

    ```json
    {
      "value": {
        "id": 10,
        "name": "doggie",
        "category": {
          "id": 1,
          "name": "Dogs"
        },
        "photoUrls": [
          "string"
        ],
        "tags": [
          {
            "id": 0,
            "name": "string"
          }
        ],
        "status": "available"
      }
    }
    ```



=== "405"

    Invalid input


## Update an existing pet

Update an existing pet by Id

```
PUT /pet
```

#### Body

Update an existent pet in the store

=== "json"

    <a href="#pet">Pet</a>


=== "xml"

    <a href="#pet">Pet</a>


=== "x-www-form-urlencoded"

    <a href="#pet">Pet</a>


#### Response


=== "200"

    Successful operation

    Returns <a href="#pet">Pet</a>

    ```json
    {
      "value": {
        "id": 10,
        "name": "doggie",
        "category": {
          "id": 1,
          "name": "Dogs"
        },
        "photoUrls": [
          "string"
        ],
        "tags": [
          {
            "id": 0,
            "name": "string"
          }
        ],
        "status": "available"
      }
    }
    ```



=== "400"

    Invalid ID supplied



=== "404"

    Pet not found



=== "405"

    Validation exception


## Finds Pets by status

Multiple status values can be provided with comma separated strings

```
GET /pet/findByStatus
```

#### Query

|Field|Description|
|-----|-----------|
|`status`|<strong>string</strong> </br>Status values that need to be considered for filter|

#### Response


=== "400"

    Invalid status value


## Finds Pets by tags

Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.

```
GET /pet/findByTags
```

#### Query

|Field|Description|
|-----|-----------|
|`tags`|<strong>array</strong> </br>Tags to filter by|

#### Response


=== "400"

    Invalid tag value


## Find pet by ID

Returns a single pet

```
GET /pet/{petId}
```

#### Path

|Field|Description|
|-----|-----------|
|`petId`|<strong>int64</strong> (required) </br>ID of pet to return|

#### Response


=== "400"

    Invalid ID supplied



=== "404"

    Pet not found


## Updates a pet in the store with form data


```
POST /pet/{petId}
```

#### Path

|Field|Description|
|-----|-----------|
|`petId`|<strong>int64</strong> (required) </br>ID of pet that needs to be updated|

#### Query

|Field|Description|
|-----|-----------|
|`status`|<strong>string</strong> </br>Status of pet that needs to be updated|
|`name`|<strong>string</strong> </br>Name of pet that needs to be updated|

#### Response


=== "405"

    Invalid input


## Deletes a pet


```
DELETE /pet/{petId}
```

#### Header

|Field|Description|
|-----|-----------|
|`api_key`|<strong>string</strong> |

#### Path

|Field|Description|
|-----|-----------|
|`petId`|<strong>int64</strong> (required) </br>Pet id to delete|

#### Response


=== "400"

    Invalid pet value


## uploads an image


```
POST /pet/{petId}/uploadImage
```

#### Path

|Field|Description|
|-----|-----------|
|`petId`|<strong>int64</strong> (required) </br>ID of pet to update|

#### Query

|Field|Description|
|-----|-----------|
|`additionalMetadata`|<strong>string</strong> </br>Additional Metadata|

#### Body


=== "octet-stream"

    <strong>binary</strong> 


#### Response


## Returns pet inventories by status

Returns a map of status codes to quantities

```
GET /store/inventory
```

#### Response


## Place an order for a pet

Place a new order in the store

```
POST /store/order
```

#### Body


=== "json"

    <a href="#order">Order</a>


=== "xml"

    <a href="#order">Order</a>


=== "x-www-form-urlencoded"

    <a href="#order">Order</a>


#### Response


=== "405"

    Invalid input


## Find purchase order by ID

For valid response try integer IDs with value <= 5 or > 10. Other values will generated exceptions

```
GET /store/order/{orderId}
```

#### Path

|Field|Description|
|-----|-----------|
|`orderId`|<strong>int64</strong> (required) </br>ID of order that needs to be fetched|

#### Response


=== "400"

    Invalid ID supplied



=== "404"

    Order not found


## Delete purchase order by ID

For valid response try integer IDs with value < 1000. Anything above 1000 or nonintegers will generate API errors

```
DELETE /store/order/{orderId}
```

#### Path

|Field|Description|
|-----|-----------|
|`orderId`|<strong>int64</strong> (required) </br>ID of the order that needs to be deleted|

#### Response


=== "400"

    Invalid ID supplied



=== "404"

    Order not found


## Create user

This can only be done by the logged in user.

```
POST /user
```

#### Body

Created user object

=== "json"

    <a href="#user">User</a>


=== "xml"

    <a href="#user">User</a>


=== "x-www-form-urlencoded"

    <a href="#user">User</a>


#### Response


## Creates list of users with given input array

Creates list of users with given input array

```
POST /user/createWithList
```

#### Body


=== "json"

    <strong>array[ <a href="#user">User</a> ]</strong> 


#### Response


=== "default"

    successful operation


## Logs user into the system


```
GET /user/login
```

#### Query

|Field|Description|
|-----|-----------|
|`password`|<strong>string</strong> </br>The password for login in clear text|
|`username`|<strong>string</strong> </br>The user name for login|

#### Response


=== "400"

    Invalid username/password supplied


## Logs out current logged in user session


```
GET /user/logout
```



#### Response


=== "default"

    successful operation


## Get user by user name


```
GET /user/{username}
```

#### Path

|Field|Description|
|-----|-----------|
|`username`|<strong>string</strong> (required) </br>The name that needs to be fetched. Use user1 for testing. |

#### Response


=== "400"

    Invalid username supplied



=== "404"

    User not found


## Update user

This can only be done by the logged in user.

```
PUT /user/{username}
```

#### Path

|Field|Description|
|-----|-----------|
|`username`|<strong>string</strong> (required) </br>name that need to be deleted|

#### Body

Update an existent user in the store

=== "json"

    <a href="#user">User</a>


=== "xml"

    <a href="#user">User</a>


=== "x-www-form-urlencoded"

    <a href="#user">User</a>


#### Response


=== "default"

    successful operation


## Delete user

This can only be done by the logged in user.

```
DELETE /user/{username}
```

#### Path

|Field|Description|
|-----|-----------|
|`username`|<strong>string</strong> (required) </br>The name that needs to be deleted|

#### Response


=== "400"

    Invalid username supplied



=== "404"

    User not found




## Schemas

### Address

|Field|Description|
|-----|-----------|
|`street`|<strong>string</strong> |
|`city`|<strong>string</strong> |
|`state`|<strong>string</strong> |
|`zip`|<strong>string</strong> |

### ApiResponse

|Field|Description|
|-----|-----------|
|`code`|<strong>int32</strong> |
|`type`|<strong>string</strong> |
|`message`|<strong>string</strong> |

### Category

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`name`|<strong>string</strong> |

### Customer

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`username`|<strong>string</strong> |
|`address`|<strong>array[ <a href="#address">Address</a> ]</strong> |

### Order

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`petId`|<strong>int64</strong> |
|`quantity`|<strong>int32</strong> |
|`shipDate`|<strong>date-time</strong> |
|`status`|<strong>string</strong> </br>Order Status<br/>Values: `placed`, `approved`, `delivered`|
|`complete`|<strong>boolean</strong> |

### Pet

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`name`|<strong>string</strong> |
|`category`|<strong>null</strong> |
|`photoUrls`|<strong>array[ string ]</strong> |
|`tags`|<strong>array[ <a href="#tag">Tag</a> ]</strong> |
|`status`|<strong>string</strong> </br>pet status in the store<br/>Values: `available`, `pending`, `sold`|

### Tag

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`name`|<strong>string</strong> |

### User

|Field|Description|
|-----|-----------|
|`id`|<strong>int64</strong> |
|`username`|<strong>string</strong> |
|`firstName`|<strong>string</strong> |
|`lastName`|<strong>string</strong> |
|`email`|<strong>string</strong> |
|`password`|<strong>string</strong> |
|`phone`|<strong>string</strong> |
|`userStatus`|<strong>int32</strong> </br>User Status|


## RequestBodies

### Pet

Pet object that needs to be added to the store

=== "json"

    <a href="#pet">Pet</a>


=== "xml"

    <a href="#pet">Pet</a>


### UserArray

List of user object

=== "json"

    <strong>array[ <a href="#user">User</a> ]</strong> 



## SecuritySchemes

### api_key

<strong>apiKey</strong> 

### petstore_auth

<strong>oauth2</strong> 
