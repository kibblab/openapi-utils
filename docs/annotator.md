
# Annotator

Extract and insert annotations from JSON files.

## Extract properties

```sh
jq -rf extract.jq <file> [--arg json true] [--args prop1 prop2 ... propN]
```

- `<file>` : path to the JSON file
- `--arg yaml true` : (optional) output annotations in YAML
- `--args prop1 prop2 ... propN` : (optional) specify props to extract

### Default

```sh
jq -rf extract.jq "schemas/petstore.json" --arg yaml true > petstore.anno.yml
```

By default, the script outputs `.operationId`, `.summary`, `.required`, `.name` and `.description`:

```yaml
info:
  description: |-
    This is a sample Pet Store Server based on the OpenAPI 3.0 specification. You can find out more about
    Swagger at [http://swagger.io](http://swagger.io)...
info.license:
  name: Apache 2.0
tags:
  - name: pet
    description: Everything about your Pets
  - name: store
    description: Operations about user
  - name: user
    description: Access to Petstore orders

# EXTERNALDOCS 0 
# =================================== 
tags.0.externalDocs:
  description: Find out more

# EXTERNALDOCS 2 
# =================================== 
tags.2.externalDocs:
  description: Find out more about our store

# POST /pet 
# =================================== 
paths./pet.post:
  summary: Add a new pet to the store
  description: Need a license granted by the principal
  operationId: addPet
paths./pet.post.responses.200:
  description: Successful operation
```


### Only certain fields

To get particular properties, use `--args` and pass each field:

```sh
jq -rf extract.jq "schemas/petstore.json" --arg yaml true --args "example" "examples" > examples.yml
```

Output:

```yml
paths./pet.post.responses.200.content.application/xml:
  examples: {
      "example-1": {
        "value": {
          "id": 10,
          "name": "doggie",
          "category": {
            "id": 1,
            "name": "Dogs"
          },
          "photoUrls": [
            "string"
  ...
```

### Editing annotations

The keys are in dot notation except for leaf nodes, which are indented in YAML.

You can edit the file, add new properties, etc. For example, change `info.description` to:

```yaml
info:
  description: |-
    April fools!
```

## Insert annotations ([requires yq](https://github.com/mikefarah/yq))

Use `yq` to convert the `*anno.yml` back to json, then pipe to the `insert.jq` script:

```sh
yq eval -o=json "petstore.anno.yml" |
jq -f insert.jq --slurpfile oas "schemas/petstore.json" > petstore.annotated.json
```

Now `petstore.annotated.json` will contain the updated description:

```json
{
  "openapi": "3.0.2",
  "info": {
    "description": "April fools!",
    ...
```

### Insert multiple YAML files

[Merge YAMLs with yq first](https://mikefarah.gitbook.io/yq/operators/reduce#merge-all-yaml-files-together), then pipe to jq.

```sh
yq -o=json eval-all '. as $item ireduce ({}; . * $item )' petstore.anno.yml tagged.yml examples.yml |
jq -f insert.jq --slurpfile oas "schemas/petstore.json"
```