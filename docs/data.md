# Data Extraction

Prepare a data dictionary for people who work with JSON payloads of an API. This command will get a list of schema properties (for editing and viewing data models):

```shell
jq -rcf data.jq schemas/banking.json | yq -P | yq 'to_entries | .[] | split_doc | .[] ' > anno.data.yml
```

Output:

```yaml
#/components/schemas/Id
type: integer
format: int64
readOnly: true
---
#/components/requestBodies/Pet/content/application~1xml/schema
name:
  type: string
  description: hooray
---
#/components/schemas/ApiResponse
code:
  type: integer
  format: int32
type:
  type: string
message:
  type: string
---
#/components/schemas/Cat/allOf/1
huntingSkill:
  type: string
  description: The measured skill for hunting
  default: lazy
  enum:
    - clueless
    - lazy
    - adventurous
    - aggressive
---
```

## 2. Get header comments

Header comments indicate the parent `$ref` of the properties.

```shell
yq '... | headComment | select(. != "")' anno.data.yml
```

Output:

```yaml
#/components/schemas/Id
---
#/components/requestBodies/Pet/content/application~1xml/schema
---
#/components/schemas/ApiResponse
---
#/components/schemas/Cat/allOf/1
---
#/components/schemas/Category
---
#/components/schemas/Category/properties/sub
---
#/components/schemas/Dog/allOf/1
---
#/components/schemas/HoneyBee/allOf/1
---
#/components/schemas/Order
---
#/components/schemas/Pet
---
#/components/schemas/Tag
---
#/components/schemas/User
---
#/paths/~1pet~1{petId}/post/requestBody/content/application~1x-www-form-urlencoded/schema
```

([Source](https://github.com/mikefarah/yq/issues/1007))

> The API page [Edit] identifies the file line location by searching for `$ref`.
