# Use from Command Line

To use the scripts as CLI.

## Create Alias On Windows

1. Create a file called `C:/Alias/apiextract.bat`

```bat
@echo off
echo.
:: Use UTF8 encoding
chcp 65001 >NUL
set scripts=%0\..\openapi-utils
jq -L %scripts%  -cf %scripts%\add-desc.jq %1 |^
jq -L %scripts% -rcf %scripts%\extract.jq --arg yaml true > %2
```

2. Clone git repository into the folder so that your folder structure looks like:

```
C:/Alias
├─ openapi-utils/
│      add-desc.jq
│      extract.jq
│      *.jq
└─ apiextract.bat
```

3. Add `C:/Alias` to your PATH environment.
4. Use from terminal in any working directory: `apiextract petstore.json anno.yml`

## Add to profile on Linux

`~/.bashrc`

```sh
export PATH=.... :~/path/to/files
```