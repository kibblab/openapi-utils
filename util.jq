def flatten_dot:
  [paths(scalars) as $path | {
    "key": $path | join("."), "value": getpath($path)
  }] | from_entries;

def mark_all_internal:
# jq -r -f mark-all-internal.jq examples/petstore.json > output.json
.paths |=
(
  to_entries
  |
    map(
      .value |= (.[] += {"x-internal": true})
      )
  | from_entries
);

# Get local reference ($ref)
# --------------------------------------------
# Usage:
# jq -r --arg ref "#/components/schemas/Pet" 'import util.jq; get_ref(.; $ref)' schemas/petstore.json

# escaped pointer ref: When a property contains a special character, escape them
# e.g. {"/endpoint/path": {}}, the escape for `/` is `~1`
# the #ref to this object would look like "#/~1endpoint~1path"
# See https://github.com/json-schema-org/JSON-Schema-Test-Suite/blob/main/tests/draft-next/ref.json
def escape_ref: if type == "string" then gsub("~";"~0") | gsub("/";"~1") | gsub ("%";"%25") else . end;
def resolve_esc: if type == "string" then gsub("~0";"~") | gsub("~1";"/") | gsub ("%25";"%") else . end;

# Returns the path as an array for getpath()
def get_ref_arr(ref):
  ref | gsub("(#\/)"; "") | gsub("\/"; ".") | split(".")
  ;

# Gets the obj from a local reference (the value of "$ref" property)
def get_ref(refstring):
   . as $_s
  | get_ref_arr(refstring | resolve_esc) as $r |
  $_s | getpath($r)
  ;
