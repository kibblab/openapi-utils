# Markdown helper library
# ================================================================
def capitalize: (.[0:1] | ascii_upcase) + .[-(length-1):];

def _getIndents($multiplier; $spaces): [] | until(length == ( ($multiplier | tonumber) *$spaces); . + [32]);
def _getIndents($multiplier): getIndents($multiplier; 2);
#   9 = \t
#  10 = \n
#  13 = \r
#  32 = (space)
#  34 = "
#  44 = ,
#  58 = :
#  91 = [
#  92 = \
#  93 = ]
# 123 = {
# 125 = }
# https://gist.github.com/cfraizer/5ac5018ec25e4b1c181a236252ce80fa
# https://stackoverflow.com/questions/53346007/how-to-pretty-print-a-json-inside-a-string-with-jq
def pretty($inlevel; $spaces):
  explode | reduce .[] as $char (
    {out: [], indent: (_getIndents($inlevel; $spaces)), string: false, escape: false};
    if .string == true then
      .out += [$char]
      | if $char == 34 and .escape == false then .string = false else . end
      | if $char == 92 and .escape == false then .escape = true else .escape = false end
    elif $char == 91 or $char == 123 then
      .indent += [32,32] | .out += [$char, 10] + .indent
    elif $char == 93 or $char == 125 then
      .indent = .indent[2:] | .out += [10] + .indent + [$char]
    elif $char == 34 then
      .out += [$char] | .string = true
    elif $char == 58 then
      .out += [$char, 32]
    elif $char == 44 then
      .out += [$char, 10] + .indent
    elif $char == 9 or $char == 10 or $char == 13 or $char == 32 then
      .
    else
      .out += [$char]
    end
  ) | .out | implode;

def pretty: pretty(0; 2);
def pretty($spaces): pretty(0; $spaces);

# https://stackoverflow.com/a/66813728
# Takes an object and makes it into a Markdown table
# Or, turn an array of the same objects into a string.
def _tableSep:
"\n|" + ( keys_unsorted | map(. | tostring | if length > 3 then length*"-" else "---" end )
| join("|")) + "|\n";

def TABLE:
  . as $a |
  if (. | type == "array") then
  . | (.[0]
      | ( "|" + ([keys_unsorted[] | capitalize ] | join("|")) + "|" )
      + _tableSep
    ) as $header
  | $a
  | map(
    . |
      ( "|" + ([ keys_unsorted[] as $k | .[$k] | tostring ] | join("|")) + "|")
    ) as $cells
  | 
  $header +
  ( $cells | join("\n") )
  elif (. | type == "object") then
    . | "|" + (keys_unsorted | join("|")) + "|"
    + _tableSep
    + "|" + ([$a[] | tostring] | join("|")) + "|"
  else
  . | tostring
  end
;

# Creates a heading
def H($level; $text): $level*"#" + " \($text)\n\n";

# Creates a codefence.
# $indent = indentation of the whole block
# $spaces = indentation spaces (2 or 4)
def CODE($lang; $code; $indent; $spaces):
  _getIndents($indent; $spaces) | implode as $i |
  ("\n"+"\($i)```\($lang // "")\n"
  + $i + (if $lang == "json" then ($code | tojson | pretty($indent; $spaces)) else ("\($i)$code" | tostring) end)
  +"\n\($i)```\n");
def CODE($lang; $code): CODE($land, $code, 0; 2);
def CODE($code):
  "\n```\n"
  +($code | tostring)
  +"\n```\n";


# Renders Pymdown tab syntax https://facelessuser.github.io/pymdown-extensions/extensions/tabbed/
# $nest = nesting level indentation. Default: 0 (no nesting)
def TAB($title; $content; $nest):
  ("\n\($nest*4*" " // "")=== \"\($title)\"\n\n"
  + "\(($nest+1)*4*" ")\($content)\n");

def TAB($title; $content): TAB($title; $content; 0);

# renders text or nothing if null
def optional($val; $linebr): if ($val != null and $val != "") then "\($val)" + $linebr*"\n" else "" end;
def optional($val): optional($val; 1);


