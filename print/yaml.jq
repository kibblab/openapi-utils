import "print/md" as MD;
# Convert json to YAML printer

# Indent multiline strings
# $tabspace = How many spaces to prepend to each line
def tab_lines($tabspaces):
  if type == "string" then
  ($tabspaces*" ") as $indent
  | split("\n") | join("\n"+$indent)
  else
  .
  end;

# Check if value needs to be in quotes and give it quotes
def wrapquotes:
  if type == "string" and test("[:\"#]") then
    "\"" + (. | gsub("\""; "\\\"") ) + "\""
  else
    .
  end;

# Creates the right hand side of the colon
def rhs($spaces):
# Output a pipe separator if multiline string block
  if (type == "string") then
    (if contains("\n") then "|-\n"+($spaces*" ") + .
    else . | wrapquotes end)
  else
    . | tostring | MD::pretty($spaces / 2; 2)
  end;

# Experimental
# ===============================================

def bothSidesPresent:
  if (has("lhs") and has ("rhs")) then
    .yaml += ["\(.lhs): \(.rhs)"]
    | del(.["lhs", "rhs"])
  else . end
;

def combineLhs:
  . as $o |
  $o | group_by(has("lhs")) |
  map(
    if (first | has("lhs"))
    then reduce .[] as $line({};
    .yaml += ["\($line.lhs): \($line.rhs)"]
    )
    else
    .
    end
  )
;

def generate:
  if has("yaml") then
    (if has("path") then
      (.path | length)*2*" "
    else
      ""
    end) as $spaces
    | . += {
      rhs: ("\n\($spaces)" + (.yaml | join("\n\($spaces)")))
      } | del(.yaml)
  else . end
  |
  if (.path | length > 0) then
    # generate lhs
    . += {lhs: .path[-1]} |
    . |= (.path = .path[:-1]) |
    if (.path | length == 0) then del(.path) else . end
  # elif (has("rhs") and has("path") and (has("lhs") | not) ) then
  else
    del(.path)
  end
;

def definePrint:
  map(
  if (.path | length == 1) then
    ((.path | length)*2*" ") as $sp |
     .print = ("\(.path | first):\n\($sp)" + (.lines | join("\n\($sp)")))
     | del(.path, .lines)
  elif (.path | length == 0) then
    (.print = (.lines | join("\n"))
     | del(.path, .lines))
  else . end
  )
;

def remainingPaths:
  . as $scals |
  reduce $scals[] as $level([]; . += [$level.path] ) | unique
  | to_entries
  
  | map( . as $e | [$scals[]|select(.path == $e.value) | bothSidesPresent] 
  ) | reduce .[] as $e([]; . += [{path: $e | first | .path, lines: [$e[].yaml] | add }])
  | definePrint
;

# Convert a json value to indented YAML
def formatObj:
  . as $_o | [paths(scalars)] | map(
    . as $to_s |
    ($_o | getpath($to_s) | rhs(4)) as $_v |
    {path:$to_s[:-1], rhs:$_v, lhs: $to_s[-1]}
    )
    | remainingPaths
    #| group_by(.path | first) | reduce .[] as $level([];
     # . += [ $level[] | bothSidesPresent | generate ] )
    
    # | group_by(.path[0]) as $groups |
    # $groups
    # (
    #   reduce $groups[] as $g({}; (
    #   $g | first | .path[0]) as $k |
    #   .[$k] += [$g[] | . |= (.path = .path[1:])] ) 
    # ) as $topped
    #   | $topped | keys_unsorted

      #[] | .[] |= (. | prune) | combineLhs

  ;
  # ([ keys_unsorted[]
  #   | ($_o[.] | tab_lines(4) | rhs(4)) as $_v
  #   | "\(.): " as $_k
  #   | $_k + $_v
  # ] | join("\n"));

# ====================================

# Convert a json value to indented YAML
def formatValueYaml($val):
  ([ keys_unsorted[]
    | ($val[.] | tab_lines(4) | rhs(4)) as $_v
    | "\(.): " as $_k
    | $_k + $_v
  ] | join("\n  "));

def formatToYaml($arr):
  if ($arr | type == "array" and ($arr | first | type == "object") ) then (
    # creates YAML object array formatting
    # i.e [{name: hello, foo:bar}, {name: second, foo:zaz}]
    # turns into
    # - name: hello
    #   foo: bar
    # - name: second
    #   foo: zaz
    [$arr[] | to_entries |
      ("- \(first.key): \(first.value | wrapquotes)" +
      if (. | length > 1) then
        "\n" + (.[1:] | map(. | "\(.key): \(.value | wrapquotes)")
        | join("\n"))
      else
        ""
      end
      ) | tab_lines(4)
    ] | join("\n  ")
  ) else
    formatValueYaml($arr)
  end;

# Convert json to yaml. Usage: `{name: "cursed"} | yaml`
def yaml:
  with_entries(
    .value as $val
    # Turn child objects into a stringified YAML output
    | .value |= (
        . = (formatToYaml(.) )
        )
    )
  | to_entries[] |
    if ( .key | length > 0 ) then
      # (.key | test("\\.")) as $bSpace |
      "\(.key):", "  "+.value
    else
      .value
    end;