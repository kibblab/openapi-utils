include "md";
#                       HTML
# ---------------------------------------------------

# Replace line breaks with HTML <br/> tag
def br:
  gsub("\n";"<br/>")
;

# Creates a heading from $ref
def refAnchor($ref):
"\($ref | split("/") |  .[0] + .[-1] | ascii_downcase)";

def refLink($ref):
  "<a href=\"\(refAnchor($ref))\">\($ref | split("/") | .[-1] )</a>"
;

def requiredStar:
  "<span style='color:red;user-select:none;'>*</span>"
;

def _item_desc:
  if ."$ref" then
    refLink(."$ref")
  else
    (.format // .type)
  end
;

# Include enum and required * tags
def friendly_caption:
  (if .enum then (.enum | join("`, `") | "<br/>Values: `\(.)`") else "" end) as $_e
  | (if .required then "(required)" else "" end) as $_r
  | (if .items then "[ \(.items | _item_desc) ]" else "" end) as $_items
  | (if ((.description | length) > 0) then "</br>\( .description | br)" else "" end) as $_d
  | "<strong>\(.format // .type)\($_items)</strong> \($_r)\($_d)\($_e)"
;

def exampleMd($obj):
  $obj |
  if has("examples") then 
    "\n" + ([
      # .examples[]? | CODE("json"; .; 1; 4)
      .examples | to_entries[]
      | "\n    **\(.key)**\n" + CODE("json"; .value; 1; 4)]
      | join(""))
    elif has("example") then
      "\n" + CODE("json"; .example; 1; 4)
    else
    ""
    end;

def exampleRes($resCode):
  $resCode | ..|select(.example or .examples)?
  | exampleMd(.)
;

# takes a schema with $ref and returns an anchor link to the $ref
# or a description
def _handleRef:
  if .schema."$ref" then refLink(.schema."$ref")
  else (.schema | friendly_caption) end
;

# Parameters array, separate by .in type 
# https://swagger.io/docs/specification/describing-parameters/
# ----------------------------------------------------------
# --- Returns an object: 
# {
#   key: "query" | "header" | "path" | (the value of parameter.in)
#   value: [
#     { name: "", required: true, type: "", description: ""},
#     { ... }
#   ]
# }
def organizeParameters($arr):
  $arr | group_by(.in) as $groups
  | $groups | map(
    {
      key: (.[0].in),
      value: .
        | map({
          field: "`\(.name)`\(if .required then requiredStar else "" end)", # add backticks for Markdown
          required: (.required // false),
          type: (if .schema."$ref" then refLink(.schema."$ref") else (.schema.format // .schema.type) end),
          description
          })
      }
    );

# Render parameter to md table
def parameterMd($params):
$params | organizeParameters($params)
  | map(
    . | "#### \( .key | capitalize )"
    + "\n\n"
    + (.value | sort_by(.required) | reverse
        | map(
        # Beef up the description
          .description = (. | friendly_caption)
        # Only keep these
        | {field, description}
        # Render a table
        ) | TABLE
     )
  ) | join("\n\n");

# requestBody object
# https://swagger.io/specification/#request-body-object
def rqBodyMd($obj):
  $obj | optional($obj.description)
  + if $obj.content then
    $obj.content | to_entries
    | map(. 
      | exampleMd(.value) as $_ex
      | "\( .key | sub("application/";"") )" as $_head # remove "application/ for aesthetic purposes"
      | .value | _handleRef as $_ref
      | TAB($_head; $_ref + $_ex)
    )
    | join("\n")
  else "" end;

# Response
# https://swagger.io/docs/specification/describing-responses/
def responseMd($res):
  optional($res.description)
  + if $res.content then
    $res.content[
      ($res.content | keys | .[0]) # only get the first property
      ]
    | exampleRes($res) as $_e
    | "\n    Returns " + _handleRef +  $_e
        # else "    " + CODE("json"; .; 1; 4) end)
  else "" end;

# Render Operation Object to md
# optional $outsideParams because params can be in the operation object instead 
# https://swagger.io/specification/#operation-object
def operationMd($endpoint; $outsideParams):
  # Heading
  H(2; (if .value.summary then .value.summary
    elif .value.operationId then .value.operationId
    else "Missing Summary" end)
  ) 
  # Description
  + optional(.value.description)
  # GET|POST|PUT|DELETE|PATCH /endpoint block
  + CODE("\(.key | ascii_upcase) \($endpoint)")
  # Parameter block
  + (if (.value.parameters or $outsideParams) then
  "\n" + parameterMd(.value.parameters // $outsideParams) + "\n"
    else "" end)
  # Body block
  + if .value.requestBody
    then "\n"+ H(4; "Body") + rqBodyMd(.value.requestBody) + "\n"
    else "" end
  # Response block
  + if .value.responses
    then "\n" + H(4; "Response")
    # Create tabs for each Response Code (200/400/404 etc)
    + (.value.responses | to_entries
      | map(
        . | TAB(.key; responseMd(.value))
        ) | join("\n")
      )
    else "" end
;

# Schema object
# https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.3.md#schemaObject

def allOfLinks:
  (if .value.allOf then
    .value |= (
      . += {
        type: (. | [ .allOf[] | refLink(."$ref") ] | join(", ")),
      }
    )
  else . end)
;

def organizeProperties:
  if .enum then (
    [{
      enum: . | friendly_caption,
    }]
  )
  elif .content then
    rqBodyMd(.)
  elif .properties then
  (
  .properties as $_props |
  (if .required then
  # add `required` field to every property listed in required
    reduce .required[] as $item ([];
      $_props[$item] | . |= (. += {required: true}))
  else . end)
    | $_props | to_entries | map(
       allOfLinks |
      {
        field: "`\(.key)`\(if .value.required then requiredStar else "" end)",
        type: (.value.format // .value.type),
        description: .value | friendly_caption,
        sub: (.value.properties // null)
      }
      | del(.type)
      | if .sub then .sub = (.sub | tostring) else del(.sub) end
    )
  )
  else . | friendly_caption end
;

#                       Main
# ---------------------------------------------------

def paths:
.paths | to_entries[]
  | .key as $endpoint
  | .value as $val
  | $val | to_entries[]
# IN(...) is not really documented but it's here
# https://github.com/stedolan/jq/blob/80052e5275ae8c45b20411eecdd49c945a64a412/src/builtin.jq#L264
  | select(.key | IN("get", "put", "post", "delete", 
         "options", "head", "patch", "trace")
  ) | operationMd($endpoint; ($val.parameters // false))
;

# For rendering components
def components:
 .components | to_entries[] | (
  "\n",
  H(2; .key | capitalize) +
  (.value | to_entries | sort_by(.key)
    | map(
      H(3; .key) +
      (.value | organizeProperties | TABLE)
    )
    | join("\n\n")
  )
);
