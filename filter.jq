# Usage:
# jq -f filter.jq --rawfile list <includes> <oasPiped>
. as $_sch
| ($list 
    | gsub("[\\p{Cf}]"; "")  # Remove unwanted hidden characters
    | split("\n")
  )
| map( # Turn each line into an array of tokens [word, endpoint]
  . | split(" ") | select(length > 0) # Filter empty elements
)
# ----- Combine duplicate path entries
| reduce .[] as $i ({};
  if $i | length > 1 then
    .[$i[1]] += [ ($i[0] | ascii_downcase) ]
  elif $i | length == 1 and $_sch.paths[$i[0]] != null then
    .[$i[0]] = ($_sch.paths[$i[0]] | keys)
  else . end)
| . as $pathsarr # ----- $pathsarr structure:
          # {
          #   "/pets/{id}": [ "delete", "get" ],
          #   "/pets": [ "get" ]
          #   "/pet/{petId}": [] # blank array means to include all
          # }
| to_entries | map(
    $_sch.paths[ (.key) ] as $_P # Access .paths.{url}
    | # -- Create the object
    {
      "key": .key,
      "value": (
          .value
      # Turn the methods into object
          | map(
              {
                "key": . ,
                "value": ( $_P[.]
                # delete x-internal property if it exists
                  | del(.["x-internal"]?) 
                # set to external
                  | .["x-external"] = true
                  )
              } 
            )
          | from_entries
      )
    }
  )
| from_entries as $inclpaths
| $_sch
# Replace .paths with new object
| setpath(["paths"]; $inclpaths)