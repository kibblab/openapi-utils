include "print/yaml";

# Gathers all property names

# If a JSON property name has slashes, i.e.:
# { "/api/endpoint": {} }
# replace "/" in $ref with "~1"
# See https://github.com/json-schema-org/JSON-Schema-Test-Suite/blob/main/tests/draft-next/ref.json

def processSem($doc):
([path(..|select(.properties)?)] | unique) as $_o |
([path(..|select(.parameters)?)] | unique) as $_o1 |
$doc as [$info] ?// {$info} | $info.title as $title |
($_o + $_o1) |
  map( . as $p
   | {context: ( [ (if $p | first | type == "number" then $p[1:] else $p end) | .[] | if type == "string" and contains("/") then gsub("/";"~1") else . end] | "\($title)#\( . | join("/"))" )} as $_c
   | ( $doc | getpath($p) |
      if .properties then .properties | to_entries
      else .parameters? | map( {key: .name, value: {description: .description, type: .schema.type}} ) end
    ) as $f
    | $f[] | . |= ( .value += $_c )
 )
;

[.[] | processSem(.)] | add as $dict | $dict |

if ($ARGS.named.x) then
  sort_by(.key | ascii_downcase)
  | (reduce (.)[] as $s ({}; .[$s.key] |= ( ( . + [$s.value])  | unique ) ))
  | yaml
else
  group_by(.key)
  | [ .[] | {key: (first.key), count: (length) } ] | sort_by(.count) | reverse[] | "\(.key)\t\(.count)"
end
