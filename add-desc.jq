# https://stackoverflow.com/questions/50302184/how-to-add-an-element-to-a-list-only-when-it-is-not-exists-already-if-the-list-i
def ensure_has($key; $value):
  if type != "object" or has($key) then .
  else .[$key] += $value
  end ;

# Adds $key:$value to every instance of $property
# if $property is an array, then add the $key to to each child
def ensure_all($property; $key; $value):
  (..|select(.[$property]?)) |= (
    if (.[$property] | type == "array") then
      .[$property] |= (.[] |= ensure_has($key; $value))
    else
      (.[$property] |= ensure_has($key; $value))
    end)
  ;

# -------------------------------------------------
"" as $text

# Add description to each model property
| .info? |= ensure_has("description"; $text)
| .components.schemas[].properties[]? |= ensure_has("description"; $text)
| .components.schemas[].allOf[]?.properties[]? |= ensure_has("description"; $text)
# Add description to each endpoint
| .paths[][] |= ensure_has("description"; $text)
| ensure_all("parameters"; "description"; $text)
| ensure_all("requestBody"; "description"; $text)
| ensure_all("requestBodies"; "description"; $text)