$DEFAULT_ANNO=".anno.yml"
$DEFAULT_OUT=".pruned.json"
$DEFAULT_INCLUDE=".include"

$source= Read-Host "Enter path to OpenAPI schema (can be remote)"
$include= Read-Host "Enter path to include list (default: ""$($DEFAULT_INCLUDE)"")"
$outputPruned=Read-Host "Output pruned file as (default: *""$($DEFAULT_OUT)"")"
$outputAnno= Read-Host "Output annotation to (default: *$($DEFAULT_ANNO))"
if (!$include){ $include=$DEFAULT_INCLUDE}
if (!$outputAnno){
  $outputAnno = (Split-Path $source -LeafBase) + $DEFAULT_ANNO
}
if (!$outputPruned){
  $outputPruned = (Split-Path $source -LeafBase) + $DEFAULT_OUT
  Write-Host "Writing pruned JSON to """$($outputPruned)""""
}
if ($outputAnno -like "*.yml") {$extract_args=@('--arg', 'yaml', 'true')}
if ($source -like '*http*') {
  Write-Host 'Fetching remote...'
  $pruned = curl $source |
  jq -cf filter.jq --rawfile list $include |
  jq -cf prune.jq |
  jq -cf add-desc.jq
} else {
  Write-Host 'Loading local JSON...'
  $pruned = jq -cf filter.jq --rawfile list $include $source |
  jq -cf prune.jq |
  jq -cf add-desc.jq
}
$pruned | Out-File $outputPruned
Write-Host "Writing annotations to """$($outputAnno)""""
$pruned | jq -rf extract.jq $extract_args > $outputAnno