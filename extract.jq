include "print/yaml";
# xtract properties from an OpenAPI and output to YAML
import "util" as UTIL;

def printPathComment:
  . | to_entries |
  reduce .[] as $item("";
    $item.value as $_v |
    if ($_v | type == "string") then
      if $_v | inside("getpostputpatchdelete") then
      . |= ( "\($_v | ascii_upcase) \(.)" )
      # "\n# \($path[2] | ascii_upcase) \($path[1]) \n# \(35*"=") \n" else "" end) as $spacer
      else (. |= ("\($_v) \(.)")) end
    else "" end
  ) | "\n# \(.)\n# \(35*"=") \n"
;

# def properties:
# ;


# Returns an obj that lies at the end of the path
# {dot.notation.to.leaf: { description: value, summary: value ... }}
def getCondensed($path; $KEEP):
  (if ($path | length < 4) then ($path | printPathComment) else "" end) as $spacer |
  ( $spacer + ($path | join("."))) as $_k | # dot.notation.to.leaf
  getpath($path) |
  ( if (. | type == "array") then
    [.[] | del( .[(keys_unsorted - $KEEP)[]] )]
    else
    . | ( . | del( .[(keys_unsorted - $KEEP)[]] ) ) # only keep properties listed in $KEEP
    end
  ) as $_v |
  { ($_k): $_v };

# .components.schemas.Tag | formatObj

($ARGS.positional | select(length > 0) // ["summary", "operationId", "name", "description", "required"]) as $KEEP |
. as $doc |
 # only select objects that have one of the $KEEP properties
[path(..|select( keys[] | IN($KEEP[]) )? )] |
[ .[] | if (.[-1] | type == "number") then del(.[-1]) else . end] | unique as $paths |
reduce .[] as $path({};
  . += ($doc | getCondensed($path; $KEEP) )
  ) |
if ($ARGS.named.yaml) then
  yaml
else
  .
end

