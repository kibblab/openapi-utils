# OpenAPI Utility Scripts

Scripts for use with [jq](https://stedolan.github.io/jq/) and [yq](https://github.com/mikefarah/yq).

If you're on Windows, make sure you have PowerShell 7+, and your PowerShell outputs with proper encoding:

```powershell
$PSDefaultParameterValues['Out-File:Encoding'] = 'utf8'
```

## Documentation

- [Sanitize and Prune an OpenAPI schema](./docs/prune.md)
- [Annotator](./docs/annotator.md)
- [Field Glossary](./docs/fglossary.md)
- [CLI setup](./docs/cli.md)
- [Markdown Printer](./docs/markdown.md)
- [Miscellaneous](./docs/misc.md)

## Example

Sanitize an OpenAPI file to only have the endpoints specified in `.include` and extract summary+descriptions into a `*.anno.yml` file.

```powershell
$schema="schemas/petstore.json";
jq -cf filter.jq --rawfile list ".include" $schema |
jq -cf prune.jq |
jq -cf add-desc.jq |
jq -rcf extract.jq $schema --arg yaml true > "petstore.anno.yml"
```

```powershell
$schema="schemas/petstore.json";
jq -cf add-desc.jq |
jq -rcf extract.jq $schema > "petstore.anno.yml"
```

Edit `petstore.anno.yml` as needed in your text editor.

Then, make a copy of the schema, insert annotations into it, and output a Markdown page:

```powershell
$copy="petstore.copy.json";
cp schemas/petstore.json $copy &&
yq eval -j 'petstore.anno.yml' |
jq -cf insert.jq --slurpfile oas $copy |
jq -L print -rf markdown.jq $copy > 'docs/markdown_example.md'
```

> If using yq v4.13, use the `-o=json` flag instead of `-j`
>
> ```sh
> yq eval -o=json "petstore.anno.yml"
> ```

Powershell script to get annotations: 

```powershell
$> .\get_anno.ps1
Enter path to OpenAPI schema (can be remote): schemas/petstore.json
Enter path to include list (default: ".include"): .include
Enter annotation output (default: *.anno.json): petstore.anno.json
```