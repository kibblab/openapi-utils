# Usage:
# gloss -x **/swagger.json > dict.yml
# gloss **/swagger.json > counts.txt
param(
   [Alias("x")]
   [switch]$extended
)
# Set to UTF8 encoding
$PSDefaultParameterValues['Out-File:Encoding'] = 'utf8'
$scripts="$PSScriptRoot\openapi-utils"

if($args[0].Contains('*')){$in=(Get-ChildItem $args[0]).FullName} else {$in=$args[0]}
if($extended){$xx='x' } else {$xx='na'}
jq -L $scripts -rf $scripts\glossary.jq --arg $xx --slurp $in
