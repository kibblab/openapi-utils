import "./util" as UTIL;

def toRef(path): path | [ .[] | UTIL::escape_ref ] | join("/") | "#/" + . ;

. as $doc |
([ path(..|select(.properties)?) ] | unique )
| map( . as $_p | 
  {
    (toRef($_p)): (
      ($doc | getpath($_p)).properties as $datum
      | $datum | walk(if type == "object" and has("properties") then 
          del(.properties)
          else . end )
    )
  }
) as $schemas |
$doc | ([ path(..|select(.type)?) ]
| map(
    select(
      contains(["schemas"]) 
      and (contains(["properties"]) | not)
    ) as $_p
    | if ($doc | getpath($_p) | has("properties") | not) then
      $_p
      else empty end
  )
) | map( . as $_p | 
  {
    (toRef($_p)): (
      ($doc | getpath($_p))
    )
  }
) as $singles
# $singles are lone schemas with only 1 datum such as
# the schema of Id in `petstore_extended.json`
#components/schemas/Id
| $singles + $schemas | add


