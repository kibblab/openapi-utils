# Checks each element in array and converts it to number if applicable
def toNum(arr):
if (arr | contains(["responses"])) then
# Do not convert reponses because the numbers are actually object property keys
  arr
else
  [
    arr[] as $o
    | try ($o | tonumber)
      catch $o
  ]
end;

# Unflattens a json dot notation obj
# Output a JSON object with nested hierarchy
def unflatten(obj):
  obj | to_entries
  | reduce .[] as $entry (
      {}; . | setpath( toNum( $entry.key | split(".") ) ; $entry.value)
    )
;

# Recursively meld a and b,
# concatenating arrays and favoring b when there is a conflict 
# https://stackoverflow.com/questions/53661930/jq-recursively-merge-objects-and-concatenate-arrays
def meld(a; b):
  a as $a | b as $b
  | if ($a|type) == "object" and ($b|type) == "object"
    then reduce ([$a,$b]|add|keys_unsorted[]) as $k ({}; 
      .[$k] = meld( $a[$k]; $b[$k]) )
    elif ($a|type) == "array" and ($b|type) == "array"
    then
      if ($a | first | type) == "object"
      then [JOIN(INDEX($b[]; .name); $a[]; .name; .) | add]
      else $a+$b
      end
    elif $b == null then $a
    else $b
    end;

unflatten(.)
|  meld($oas[0]; .)